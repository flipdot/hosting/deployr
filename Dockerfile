FROM golang:alpine

RUN apk --no-cache add git
RUN go get -u github.com/hetznercloud/cli/cmd/hcloud

FROM docker.io/library/docker:stable

COPY --from=0 /go/bin/hcloud /usr/local/bin/hcloud
RUN apk --no-cache add docker-compose openssh-client bash ansible sshpass bash-completion
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

RUN echo "source /etc/profile.d/bash_completion.sh" >> ~/.bashrc
RUN echo "source <(hcloud completion bash)" >> ~/.bashrc

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/bin/bash"]
