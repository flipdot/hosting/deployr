#!/usr/bin/env bash
set -e

SWARM_MANAGER="swarm-manager1"
WAIT_FOR_MANAGER_SLEEP=3 # Seconds to wait between every connection attempt
WAIT_FOR_MANAGER_MAX_TRIES=60

if [[ -z "$SSH_PRIVATE_KEY" && -z "$SSH_AUTH_SOCK" ]]; then
  echo "Error: SSH_PRIVATE_KEY and SSH_AUTH_SOCK are undefined. One of them is needed."
  exit 1
fi

if [[ -z "$SWARM_MANAGER_IP" && "$HCLOUD_TOKEN" ]]; then
  if [[ -z "$HCLOUD_TOKEN" ]]; then
    echo "Error: HCLOUD_TOKEN is undefined."
    exit 1
  fi

  if ! SWARM_MANAGER_IP=$(hcloud server ip "$SWARM_MANAGER"); then
    echo "Create Server: $SWARM_MANAGER"
    hcloud server create --image "ubuntu-18.04" --type "cx11" --name $SWARM_MANAGER &> /dev/null
    SWARM_MANAGER_IP=$(hcloud server ip "$SWARM_MANAGER")
  fi
  export SWARM_MANAGER
  export SWARM_MANAGER_IP
fi

if [[ -z "$SWARM_MANAGER_IP" ]]; then
  echo "\$SWARM_MANAGER_IP is empty. Could not determine Swarm Manager IP"
  exit 1
fi

export DOCKER_HOST="ssh://$SWARM_MANAGER_IP"

if [[ "$SSH_PRIVATE_KEY" ]]; then
  eval "$(ssh-agent -s)" &> /dev/null
  ssh-add -q <(echo "$SSH_PRIVATE_KEY")
fi

mkdir -p ~/.ssh
# echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config # Should be unnecessary with the keyscan
tries_left=$WAIT_FOR_MANAGER_MAX_TRIES
until ssh-keyscan "$SWARM_MANAGER_IP" 2> /dev/null > ~/.ssh/known_hosts; do
  if [[ "$tries_left" -le "0" ]]; then
    echo -e "Error: ssh-keyscan failed for $WAIT_FOR_MANAGER_MAX_TRIES times over" \
    $((WAIT_FOR_MANAGER_MAX_TRIES * WAIT_FOR_MANAGER_SLEEP)) \
    "seconds; exiting."
    exit 1
  fi
  tries_left=$((tries_left - 1))
  sleep $WAIT_FOR_MANAGER_SLEEP
done

if ! SSH_CHECK=$(ssh -o PasswordAuthentication=no -o BatchMode=yes "$SWARM_MANAGER_IP" exit 2>&1) \
  && [[ "$SSH_CHECK" =~ "Permission denied" ]]; then
  echo "SSH Pubkey authentication failed with permission denied on $SWARM_MANAGER_IP"
  if [[ $HCLOUD_TOKEN ]]; then
    echo "Reset Root Password of $SWARM_MANAGER and generate a new one"
    SERVER_ROOT_PASSWORD=$(hcloud server reset-password "$SWARM_MANAGER" | cut -f2 -d: | tr -d "[:space:]")
    SSH_PUBKEY_TO_ADD=$(ssh-add -L | head -n1)
    echo "Adding to authorized_keys: $SSH_PUBKEY_TO_ADD (first entry of ssh-add -L)"
    sshpass -p "$SERVER_ROOT_PASSWORD" \
      ssh -o PreferredAuthentications=password \
        -o PubkeyAuthentication=no "$SWARM_MANAGER_IP" \
      echo "$SSH_PUBKEY_TO_ADD" \>\> ~/.ssh/authorized_keys
    ssh -o PasswordAuthentication=no -o BatchMode=yes "$SWARM_MANAGER_IP" exit
  fi
elif [[ -z $SSH_CHECK ]]; then
  : # Do nothing, everything is ok
else
  echo "SSH Pubkey authentication failed with: $SSH_CHECK"
fi

exec "$@"
