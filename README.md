# Deployr

A fancy Dockr base image for your deployment to the Hetznr Cloud.

## Usage

This image bundles some tools to ease your deployment, like

- Docker
- Docker Compose
- Ansible
- OpenSSH

The deployment environment (inside the Deployr container) needs
environment variables which define the Docker host to deploy to AND the
SSH credentials needed to connect.

- To target an **existing** Dockerhost either define
  - `SWARM_MANAGER_IP` to reference the Dockerhost directly
  - XOR Hetzner CLI (hcloud) credentials to retrieve the
    `SWARM_MANAGER_IP` automatically using
    - `HCLOUD_TOKEN`, the Hetzner Cloud API token
    - AND `SWARM_MANAGER` (default: `swarm-manager1`), the Hetzner Cloud
      instance name
- To authenticate you can either define
  - `SSH_PRIVATE_KEY`, to echo it into the ssh-agent inside the
    container
  - XOR `SSH_AUTH_SOCKET` (e.g. with `docker run` args
    `--volume $SSH_AUTH_SOCK:/mnt/ssh-agent:ro --env SSH_AUTH_SOCK=/mnt/ssh-agent`).
    **This method is intended to be used locally.**

With these variables the default `ENTRYPOINT` will configure the
`DOCKER_HOST` for you, so you just need to `docker run` or
`docker-compose up` your app within the container.

Environment variables for the **container/app you want to deploy** need to be
passed to the Deployr container first, afterwards you can hand them over. This
is described in the following sections.

### Gitlab-CI

Created primarily for this group, simply use this image in your deploy
stage within your `.gitlab-ci.yml` like the following example to spin up
a `docker-compose.yml`:

```yaml
deploy:
  image: registry.gitlab.com/flipdot/hosting/deployr:latest
  stage: deploy
  script:
    - docker-compose up --detach --force-recreate
```

Within this namespace (flipdot/hosting), the REQUIRED environment
variables are already set:

- `HCLOUD_TOKEN` and `SWARM_MANAGER`
- and the `SSH_PRIVATE_KEY` for the cloud instance

To pass on environment variables to the **container/app you want to
deploy** you can define them as described in the
[GitLab CI/CD docs](https://docs.gitlab.com/ee/ci/variables/README.html).
Afterwards, you can use them in Deployr to hand them over to your app
using `docker run -e VARIABLE -e PUBLIC_PASSWORD` for `$VARIABLE` and
`$PUBLIC_PASSWORD` or just defining them in a
[Compose file](https://docs.docker.com/compose/environment-variables/).

```yaml
app:
  environment:
    - MY_MOOD_IS=$VARIABLE
    - PUBLIC_PASSWORD
```

### Locally testing Deployr

To develop and test Deployr locally a git `post-commit` hook is included.
It executes the defined GitLab CI jobs except of `build-*` and builds
the Dockerfile locally, removing the resulting image afterwards.

In order to use it, you need

- A GNU userland, including the GNU `sed`
  - On FreeBSD execute `pkg install gsed`
  - On macOS execute `brew install gnu-sed` and repent
- Docker OR Podman
- GitLab Runner

To install the script as a git `post-commit` hook execute

```bash
ln -s '../../.hooks/post-commit.sh' .git/hooks/post-commit
```

Installed as a git hook as described above, the script will run with
every commit. Alternatively you can just run it manually by calling it
like `.git/hooks/post-commit`.

To skip the tests, set the environment variable `$SKIP_TESTS` to true like
`SKIP_TESTS=true git commit` while commiting.

## Contributing

Merge requests are welcome. Review upon request or resolved WIP status.
Please commit often, commit early (especially try to commit small logical
units) for easier reviews.

You SHOULD open an issue for feature requests and non-trivial changes,
from there it is easy to generate merge requests and branches.

## License

Deployr  
Copyright (C) 2020 flipdot/C3PB IT Task Force

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any
later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
