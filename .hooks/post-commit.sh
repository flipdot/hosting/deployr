#!/bin/sh
set -e

if [ "$SKIP_TESTS" = true ]; then
  exit 0
fi

echo "Running post-commit checks"
CONTAINER_IMAGE="deployer:post-commit-test"
CONTAINER_ENGINE=$(command -v docker 2> /dev/null \
  || command -v podman 2> /dev/null \
  || (echo "Error: Neither docker nor podman found in path." >&2; \
    exit 1))
SED_COMMAND=$(command -v gsed 2> /dev/null \
  || command -v sed 2> /dev/null \
  || (echo "Error: Neither gsed nor sed found in path." >&2; \
    exit 1))

# Search for all top-level elements in the YML
# Print them without the colon to get a list of jobs and keywords
$SED_COMMAND -n -E 's/^(\S+):$/\1/p' < .gitlab-ci.yml | while read -r job; do
  # Filter out reserved keywords and build stages
  if ! grep -q "$job" .hooks/keywords && ! echo "$job" | grep "build-"; then
    echo "Executing job '$job' with GitLab Runner"
    gitlab-runner exec docker "$job"
  else
    echo "Skipping '$job'"
  fi
done

echo "Building image as $CONTAINER_IMAGE"
$CONTAINER_ENGINE build --tag $CONTAINER_IMAGE .
echo "Removing image $CONTAINER_IMAGE"
$CONTAINER_ENGINE rmi $CONTAINER_IMAGE
echo "Finished post-commit checks without errors."
